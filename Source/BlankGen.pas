Unit BlankGen;

{$IFDEF FPC}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Graphics, GraphType, CompImages, LCLCompImages,
  Generators, Pallets, CPUTime;

Type
  TBlankGenerator = Class(TGenerator)
  Public
    Procedure DoRender(Image: TCompactImage; VR, VI, VS: Double); Override;
  End;

Implementation

Procedure TBlankGenerator.DoRender(Image: TCompactImage; VR, VI, VS: Double);
var
  StartX, MidX, EndX: Integer;
  StartY, MidY, EndY: Integer;
  X, Y: Integer;
  RawImage: TRawImage;
  ScanLinePointer: PByte;
  PixelPointer: PByte;
  BytesPerPixel: Integer;
  BytesPerLine: Integer;
Begin
  RawImage := GeneratorController.ImagePainter.RawImage;
  BytesPerPixel := RawImage.Description.BitsPerPixel Div 8;
  BytesPerLine := RawImage.Description.BytesPerLine;
  ScanLinePointer := RawImage.Data;
  StartX := 0;
  EndX := Image.Width-1;
  StartY := 0;
  EndY := Image.Height-1;
  For Y := StartY To EndY Do
    Begin
      PixelPointer := ScanLinePointer;
      For X := StartX To EndX Do
        Begin
          PInteger(PixelPointer)^ := RGBToColor(255,0,0);//PalletController.Active.PalletColors[0];
          Inc(PixelPointer, BytesPerPixel);
        End;
      Inc(ScanLinePointer, BytesPerLine);
    End;
End;

Var
  Generator: TBlankGenerator;

Initialization

Generator := TBlankGenerator.Create('Blank Test Generator');

Finalization

Generator.Free;

End.

