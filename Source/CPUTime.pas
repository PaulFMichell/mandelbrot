Unit CPUTime;

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils;

Type TTickUnits = (tuTicks, tuKiloTicks, tuMegaTicks);

Type
  TCPUTimer = Packed Object
    StartTick: Int64;
    TickCount: Int64;
    Function FormatTickCount(Count: Int64; Units: TTickUnits = tuTicks; ShowUnits: Boolean = False): String;
    Function KiloCountAsText: String;
    Function MegaCountAsText: String;
    Function TickCountAsText: String;
    Procedure Mark; Assembler;
    Procedure Start; Assembler;
    Procedure Stop; Assembler;
  End;

Var
  CPUTimer: TCPUTimer;

Implementation

Function TCPUTimer.FormatTickCount(Count: Int64; Units: TTickUnits = tuTicks; ShowUnits: Boolean = False): String;
Var
  DisplayTicks: Int64;
  UnitSuffix: String;
Begin
  UnitSuffix := EmptyStr;
  Case Units Of
  tuTicks:
    Begin
      DisplayTicks := Count;
      If ShowUnits Then
        UnitSuffix := 't';
    End;
  tuKiloTicks:
    Begin
      DisplayTicks := Count div 1000;
      If ShowUnits Then
        UnitSuffix := 'Kt';
    End;
  tuMegaTicks:
    Begin
      DisplayTicks := Count div 1000000;
      If ShowUnits Then
        UnitSuffix := 'Mt';
    End;
  End;
  Result := IntToStr(DisplayTicks div 1000)+UnitSuffix;
End;

Function TCPUTimer.KiloCountAsText: String;
Begin
  Result := FormatTickCount(TickCount, tuKiloTicks, True);
End;

Function TCPUTimer.MegaCountAsText: String;
Begin
  Result := FormatTickCount(TickCount, tuMegaTicks, True);
End;

Function TCPUTimer.TickCountAsText: String;
Begin
  Result := IntToStr(TickCount);
End;

Procedure TCPUTimer.Mark; Assembler; NoStackFrame;
Asm
  {$IFDEF CPU64}
  mov rcx, Self
  rdtsc
  shl rdx, 32
  or rax, rdx
  sub rax, rcx[StartTick]
  mov rcx[TickCount], rax
  {$ELSE CPU32}
  mov ecx, Self
  rdtsc
  sub eax, ecx[StartTick]  { Subtract low 32bits first. }
  sbb edx, ecx[StartTick+4]  { Subtract high 32bits with borrow. }
  mov ecx[TickCount], eax
  mov ecx[TickCount+4], edx
  {$ENDIF CPU64}
End;

Procedure TCPUTimer.Start; Assembler; NoStackFrame;
Asm
  {$IFDEF CPU64}
  mov rcx, Self
  rdtsc
  shl rdx, 32
  or rax, rdx
  mov rcx[StartTick], rax
  {$ELSE CPU32}
  mov ecx, Self
  rdtsc
  mov ecx[StartTick], eax
  mov ecx[StartTick+4], edx
  {$ENDIF CPU64}
End;

Procedure TCPUTimer.Stop; Assembler; NoStackFrame;
Asm
  {$IFDEF CPU64}
  mov rcx, Self
  rdtsc
  shl rdx, 32
  or rax, rdx
  sub rax, rcx[StartTick]
  mov rcx[TickCount], rax
  mov rcx[StartTick], 0
  {$ELSE CPU32}
  mov ecx, Self
  rdtsc
  sub eax, ecx[StartTick]  { Subtract low 32bits first. }
  sbb edx, ecx[StartTick+4]  { Subtract high 32bits with borrow. }
  mov ecx[TickCount], eax
  mov ecx[TickCount+4], edx
  mov ecx[StartTick], 0
  mov ecx[StartTick+4], 0
  {$ENDIF CPU64}
End;

End.

