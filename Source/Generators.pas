Unit Generators;

{$IFDEF FPC}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Graphics, GraphType, IntfGraphics, LCLIntF, LCLType, Types,
  Pallets, CompImages, LCLCompImages, GLCompImages, CPUTime;

Type
  TRealPoint = Record
    R: Double;
    I: Double;
  End;

Type
  TGeneratorController = Class;
  TGenerator = Class
  Private
    FController: TGeneratorController;
    FName: String;
    FRenderTicks: Int64;
    FRenderTime: TDateTime;
  Protected
    Procedure DoRender(Image: TCompactImage; VR, VI, VS: Double); Virtual; Abstract;
    Property Controller: TGeneratorController Read FController Write FController;
  Public
    Constructor Create(Name: String);
    Destructor Destroy; Override;
    Procedure Render;
    Property Name: String Read FName;
    Property RenderTicks: Int64 Read FRenderTicks;
    Property RenderTime: TDateTime Read FRenderTime;
  End;
  TGeneratorController = Class
  Private
    FActive: TGenerator;
    FGenerators: TFPList;
    FImage: TCompactImage;
    FImagePainter: TLCLCompactImage;
    FImageGLPainter: TGLCompactImage;
    FOutputRectangle: TRect;
    FOutputTime: TDateTime;
    FPanStep: Integer;
    FSourceRectangle: TRect;
  Protected
    VR, VI, VS: Double;
    ImageAR: Double;
    CanvasAR: Double;
    Function GetCount: Integer; Virtual;
    Function GetGenerator(Index: Integer): TGenerator; Virtual;
    Function PointToRealPoint(Point: TPoint): TRealPoint;
    Procedure SetGenerator(Index: Integer; Value: TGenerator); Virtual;
    Procedure SetOutputRectangle(Value: TRect); Virtual;
  Public
    Constructor Create;
    Destructor Destroy; Override;
    Procedure Activate(Index: Integer);
    Procedure BoxZoom(ZoomRect: TRect);
    Procedure DrawToCanvas(Canvas: TCanvas);
    Procedure DrawToOpenGL(DisplayRect: TRect);
    Procedure PanLeft;
    Procedure PanRight;
    Procedure PanUp;
    Procedure PanDown;
    Procedure SaveToFile(FileName: String);
    Procedure SaveToStream(Stream: TStream);
    Procedure SetImageSize(Width, Height: Integer);
    Procedure UpdateImage;
    Procedure ZoomIn;
    Procedure ZoomInAt(X, Y: Integer);
    Procedure ZoomOut;
    Property Active: TGenerator Read FActive;
    Property Count: Integer Read GetCount;
    Property Generators[Index: Integer]: TGenerator Read GetGenerator Write SetGenerator;
    Property Image: TCompactImage Read FImage;
    Property ImagePainter: TLCLCompactImage Read FImagePainter;
    Property OutputRectangle: TRect Read FOutputRectangle Write SetOutputRectangle;
    Property OutputTime: TDateTime Read FOutputTime;
    Property PanStep: Integer Read FPanStep Write FPanStep;
    Property SourceRectangle: TRect Read FSourceRectangle;
  End;

Var
  GeneratorController: TGeneratorController;

Implementation

Constructor TGenerator.Create(Name: String);
Begin
  FName := Name;
  If GeneratorController.FGenerators.Add(Self)=0 Then
    GeneratorController.Activate(0);
End;

Destructor TGenerator.Destroy;
Begin
  Inherited Destroy;
End;

Procedure TGenerator.Render;
Var
  StartTime: TDateTime;
Begin
  StartTime := Now;
  CPUTimer.Start;
  With GeneratorController Do
    DoRender(Image, VR, VI, VS);
  CPUTimer.Stop;
  FRenderTicks := CPUTimer.TickCount;
  FRenderTime := Now-StartTime;
End;

Constructor TGeneratorController.Create;
Begin
  FActive := Nil;
  FGenerators := TFPList.Create;
  FImage := TCompactImage.Create(0, 0);
  FImagePainter := TLCLCompactImage.Create(FImage);
  FImageGLPainter := TGLCompactImage.Create(FImage);
  VR := -0.5;
  VI := 0;
  VS := 0.0025;
  FPanStep := 25;
End;

Destructor TGeneratorController.Destroy;
Var
  Index: Integer;
Begin
  FActive := Nil;
  FGenerators.Free; { Note: Individual Generators free themselves. }
  FImageGLPainter.Free;
  FImagePainter.Free;
  FImage.Free;
  Inherited Destroy;
End;

Procedure TGeneratorController.Activate(Index: Integer);
Begin
  FActive := Generators[Index];
End;

Procedure TGeneratorController.BoxZoom(ZoomRect: TRect);
Var
  ZoomCenter: TRealPoint;
  ViewCenter: TRealPoint;
  ScaleX: Double;
  ScaleY: Double;
Begin
  ZoomCenter := PointToRealPoint(ZoomRect.CenterPoint);
  { Zoom centre must be calculated prior to scale. }
  ScaleX := ZoomRect.Width/OutputRectangle.Width;
  ScaleY := ZoomRect.Height/OutputRectangle.Height;
  If ScaleX>ScaleY Then
    VS := VS*ScaleX
  Else
    VS := VS*ScaleY;
  ViewCenter := PointToRealPoint(OutputRectangle.CenterPoint);
  VR := VR-ViewCenter.R+ZoomCenter.R;
  VI := VI-ViewCenter.I+ZoomCenter.I;
  UpdateImage;
End;

Procedure TGeneratorController.DrawToCanvas(Canvas: TCanvas);
Var
  StartTime: TDateTime;
Begin
  OutputRectangle := Rect(0, 0, Canvas.Width, Canvas.Height);
  StartTime := Now();
  FImagePainter.DrawTo(Canvas);
  FOutputTime := Now()-StartTime;
End;

Procedure TGeneratorController.DrawToOpenGL(DisplayRect: TRect);
Var
  StartTime: TDateTime;
Begin
  OutputRectangle := DisplayRect;
  StartTime := Now();
  FImageGLPainter.DrawToOpenGL(DisplayRect);
  FOutputTime := Now()-StartTime;
End;

Procedure TGeneratorController.PanLeft;
Begin
  VR := VR-VS*FPanStep;
  UpdateImage;
End;

Procedure TGeneratorController.PanRight;
Begin
  VR := VR+VS*FPanStep;
  UpdateImage;
End;

Procedure TGeneratorController.PanUp;
Begin
  VI := VI+VS*FPanStep;
  UpdateImage;
End;

Procedure TGeneratorController.PanDown;
Begin
  VI := VI-VS*FPanStep;
  UpdateImage;
End;

Procedure TGeneratorController.SaveToFile(FileName: String);
Begin
  FImage.SaveToFile(FileName);
End;

Procedure TGeneratorController.SaveToStream(Stream: TStream);
Begin
  // TODO: FImage.SaveToStream();
End;

Procedure TGeneratorController.SetImageSize(Width, Height: Integer);
Begin
  FImage.SetSize(Width, Height);
  FImagePainter.Update;
  FSourceRectangle := Rect(0, 0, Width, Height);
  UpdateImage;
End;

Procedure TGeneratorController.UpdateImage;
Begin
  If Assigned(Active) Then
    Active.Render;
End;

Procedure TGeneratorController.ZoomIn;
Begin
  VS := VS/2;
  UpdateImage;
End;

Procedure TGeneratorController.ZoomInAt(X, Y: Integer);
Begin
  VS := VS/2;
  VR := VR+VS*(X-(Image.Width Div 2));
  VI := VI+VS*((Image.Height Div 2)-Y);
  UpdateImage;
End;

Procedure TGeneratorController.ZoomOut;
Begin
  VS := VS*2;
  UpdateImage;
End;

Function TGeneratorController.GetCount: Integer;
Begin
  Result := FGenerators.Count;
End;

Function TGeneratorController.GetGenerator(Index: Integer): TGenerator;
Begin
  If (Index>=0) And (Index<FGenerators.Count) Then
    Result := TGenerator(FGenerators.Items[Index])
  Else
    Result := Nil;
End;

Function TGeneratorController.PointToRealPoint(Point: TPoint): TRealPoint;
Begin
  Result.R := VR+VS*Double(Point.X-(Image.Width Div 2));
  Result.I := VI+VS*Double((Image.Height-Point.Y)-(Image.Height Div 2));
End;

Procedure TGeneratorController.SetGenerator(Index: Integer; Value: TGenerator);
Begin
  FGenerators.Items[Index] := Value;
End;

Procedure TGeneratorController.SetOutputRectangle(Value: TRect);
Begin
  FOutputRectangle := Value;
  FPanStep := FOutputRectangle.Width Div 5;
  {
  //Var
  //  NewHeight, NewWidth: Double;

  ImageAR := Double(FBitmap.Width)/Double(FBitmap.Height);
  CanvasAR := Double(ClientWidth)/Double(ClientHeight);
  ImageOutputRect := Rect(0, 0, FBitmap.Width, FBitmap.Height);
  If CanvasAR>ImageAR Then
    Begin
      NewHeight := ImageAR*ImageBuffer.Height/CanvasAR;
      ImageOutputRect.Top := Trunc((ImageBuffer.Height-NewHeight)/2);
      ImageOutputRect.Bottom := ImageOutputRect.Top+Trunc(NewHeight);
    End
  Else
    Begin
      NewWidth := CanvasAR*ImageBuffer.Width/ImageAR;
      ImageOutputRect.Left := Trunc((ImageBuffer.Width-NewWidth)/2);
      ImageOutputRect.Right := ImageOutputRect.Left+Trunc(NewWidth);
    End;}
End;

Initialization

GeneratorController := TGeneratorController.Create;

Finalization

GeneratorController.Free;

End.

