Unit Main;

{$IFDEF FPC}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Forms, Dialogs, Controls, LCLType, Menus, ActnList, ComCtrls,
  Clipbrd, ExtCtrls, Graphics, OpenGLContext, OpenGLProcs, Generators, Pallets;

Type
  TMainForm = Class(TForm)
    AboutAction: TAction;
    MenuItem1: TMenuItem;
    UseOpenGLAction: TAction;
    OpenGLControl: TOpenGLControl;
    RefreshPopupItem: TMenuItem;
    RefreshMenuItem: TMenuItem;
    RefreshBreak: TMenuItem;
    RefreshAction: TAction;
    CopyAction: TAction;
    HelpMenuItem: TMenuItem;
    AboutMenuItem: TMenuItem;
    ImagePaintBox: TPaintBox;
    SaveAction: TAction;
    ExitAction: TAction;
    FullscreenAction: TAction;
    ActionList: TActionList;
    MainMenu: TMainMenu;
    EditMenuItem: TMenuItem;
    FullscreenMenuItem: TMenuItem;
    FullscreenPopupItem: TMenuItem;
    ExitPopupBreak: TMenuItem;
    ExitPopupItem: TMenuItem;
    FileMenuItem: TMenuItem;
    FullscreenBreak: TMenuItem;
    CopyMenuItem: TMenuItem;
    MethodBreak: TMenuItem;
    SaveDialog: TSaveDialog;
    SaveMenuItem: TMenuItem;
    MenuItem3: TMenuItem;
    ExitMenuItem: TMenuItem;
    P2048MenuItem: TMenuItem;
    P256MenuItem: TMenuItem;
    P512MenuItem: TMenuItem;
    P1024MenuItem: TMenuItem;
    PalletSizeMenuItem: TMenuItem;
    PalletMenuItem: TMenuItem;
    MainPopupMenu: TPopupMenu;
    GeneratorMenuItem: TMenuItem;
    StatusBar: TStatusBar;
    Procedure AboutActionExecute(Sender: TObject);
    Procedure CopyActionExecute(Sender: TObject);
    Procedure ExitActionExecute(Sender: TObject);
    Procedure FullscreenActionExecute(Sender: TObject);
    Procedure FormCreate(Sender: TObject);
    Procedure FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure GeneratorMenuItemClick(Sender: TObject);
    Procedure OutputMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    Procedure OutputMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    Procedure OutputMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    Procedure OutputResize(Sender: TObject);
    Procedure ImagePaintBoxPaint(Sender: TObject);
    Procedure OpenGLControlPaint(Sender: TObject);
    Procedure PalletMenuItemClick(Sender: TObject);
    Procedure PalletSizeMenuItemClick(Sender: TObject);
    Procedure RefreshActionExecute(Sender: TObject);
    Procedure SaveActionExecute(Sender: TObject);
    Procedure UseOpenGLActionExecute(Sender: TObject);
  Private
    BoxDragging: Boolean;
    DragRect: TRect;
    DragStart: TPoint;
    Procedure BuildPalletMenu(ParentMenuItem: TMenuItem);
    Procedure BuildGeneratorMenu(ParentMenuItem: TMenuItem);
  End;

Var
  MainForm: TMainForm;

Implementation

Uses
  LCLIntf, LMessages;

{$R *.lfm}

Procedure TMainForm.FormCreate(Sender: TObject);
Begin
  PalletController.Add('Greyscale: Black-White', [clBlack, clWhite]);
  PalletController.Add('Greyscale: White-Black', [clWhite, clBlack]);
  PalletController.Add('Red-Green-Blue-White', [clRed, clGreen, clBlue, clWhite]);
  PalletController.Add('Pastel Colours', [clMoneyGreen, clSkyBlue, clCream, clMedGray]);
  PalletController.Add('Navy-Yellow-Red-PaleGreen-White', [clNavy, clYellow, clRed, clMoneyGreen, clWhite]);
  PalletController.Add('Default Colour', [RGBToColor($00, $00, $20), RGBToColor($FF, $FF, $FF),
                                          RGBToColor($00, $A0, $00), RGBToColor($FF, $40, $FF),
                                          RGBToColor($20, $20, $FF)]);
  PalletController.Activate(5);
  BuildPalletMenu(PalletMenuItem);
  GeneratorController.SetImageSize(Screen.Width, Screen.Height);
  BuildGeneratorMenu(GeneratorMenuItem);
End;

Procedure TMainForm.FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  Msg: TMsg;
Begin
  With GeneratorController Do
    Begin
      If ssCtrl In Shift Then
        PanStep := 10
      Else If ssShift In Shift Then
        PanStep := 50
      Else
        PanStep := 25;
      Case Key Of
      vk_Left: PanLeft;
      vk_Right: PanRight;
      vk_Up: PanUp;
      vk_Down: PanDown;
      vk_Add: ZoomIn;
      vk_Subtract: ZoomOut;
      End;
    End;
  //While PeekMessage(Msg, 0, LM_KEYFIRST, LM_KEYLAST, 1) do
   // beep;
  RePaint;
End;

Procedure TMainForm.GeneratorMenuItemClick(Sender: TObject);
Begin
  GeneratorController.Activate(TMenuItem(Sender).Tag);
  RefreshAction.Execute;
End;

Procedure TMainForm.OutputMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Begin
  DragStart.X := X;
  DragStart.Y := Y;
  BoxDragging := False;
End;

Procedure TMainForm.OutputMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
Begin
  If ssLeft In Shift Then
    Begin
      DragRect := Rect(DragStart.x, DragStart.Y, X, Y);
      BoxDragging := True;
      Repaint;
    End;
End;

Procedure TMainForm.OutputMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Begin
  If BoxDragging Then
    Begin
      BoxDragging := False;
      GeneratorController.BoxZoom(Rect(DragStart.x, DragStart.Y, X, Y));
      Invalidate;
    End
  Else If Button=mbLeft Then
    Begin
      GeneratorController.ZoomInAt(X, Y);
      Invalidate;
    End;
End;

Procedure TMainForm.OutputResize(Sender: TObject);
Begin
  If Sender=ImagePaintBox Then
    GeneratorController.OutputRectangle := ImagePaintBox.ClientRect
  Else If Sender=OpenGLControl Then
    GeneratorController.OutputRectangle := OpenGLControl.ClientRect;
End;

Procedure TMainForm.ImagePaintBoxPaint(Sender: TObject);
Begin
  If Not UseOpenGLAction.Checked Then
    Begin
      GeneratorController.DrawToCanvas(ImagePaintBox.Canvas);
      If BoxDragging Then
        With ImagePaintBox.Canvas Do
          Begin
            Brush.Style := bsClear;
            Pen.Color := clRed;
            Pen.Style := psDash;
            Rectangle(DragRect);
          End;
      StatusBar.SimpleText := GeneratorController.Active.Name+', '+
                              'Generation: '+FormatDateTime('Z', GeneratorController.Active.RenderTime)+'ms, '+
                              'Display: '+FormatDateTime('Z', GeneratorController.OutputTime)+'ms';
    End;
End;

Procedure TMainForm.OpenGLControlPaint(Sender: TObject);
Begin
  If UseOpenGLAction.Checked Then
    Begin
      OpenGLControl.MakeCurrent;
      GeneratorController.DrawToOpenGL(OpenGLControl.ClientRect);
      If BoxDragging Then
        DrawOpenGLSelectionRect(OpenGLControl.ClientRect, DragRect);
      OpenGLControl.SwapBuffers;
      StatusBar.SimpleText := GeneratorController.Active.Name+', '+
                              'Generation: '+FormatDateTime('Z', GeneratorController.Active.RenderTime)+'ms, '+
                              'Display: '+FormatDateTime('Z', GeneratorController.OutputTime)+'ms';
    End;
End;

Procedure TMainForm.PalletMenuItemClick(Sender: TObject);
Begin
  PalletController.Activate(TMenuItem(Sender).Tag);
  RefreshAction.Execute;
End;

Procedure TMainForm.PalletSizeMenuItemClick(Sender: TObject);
Begin
  PalletController.Active.PalletSize := TMenuItem(Sender).Tag;
  RefreshAction.Execute;
End;

Procedure TMainForm.AboutActionExecute(Sender: TObject);
Begin
  ShowMessage('Mandelbrot Generator'+LineEnding+'(c) Michell Computing 2016');
End;

Procedure TMainForm.CopyActionExecute(Sender: TObject);
Begin
  Clipboard.Assign(GeneratorController.Image);
End;

Procedure TMainForm.ExitActionExecute(Sender: TObject);
Begin
  Close;
End;

Var
  RestoreRect: TRect;

Procedure TMainForm.FullscreenActionExecute(Sender: TObject);
Begin
  If FullscreenAction.Checked Then
    Begin
      RestoreRect := BoundsRect;
      Menu := Nil;
      BorderStyle := bsNone;
      WindowState := wsFullScreen;
    End
  Else
    Begin
      Menu := MainMenu;
      WindowState := wsMaximized;
      BoundsRect := RestoreRect;
      BorderStyle := bsSizeable;
      WindowState := wsNormal;
    End;
End;

Procedure TMainForm.RefreshActionExecute(Sender: TObject);
Begin
  GeneratorController.UpdateImage;
  Invalidate;
End;

Procedure TMainForm.SaveActionExecute(Sender: TObject);
Begin
  If SaveDialog.Execute Then
    GeneratorController.SaveToFile(SaveDialog.FileName);
End;

Procedure TMainForm.UseOpenGLActionExecute(Sender: TObject);
Begin
  OpenGLControl.Visible := UseOpenGLAction.Checked;
  Repaint;
End;

Procedure TMainForm.BuildPalletMenu(ParentMenuItem: TMenuItem);
Var
  Index: Integer;
  NewItem: TMenuItem;
Begin
  For Index := 0 To PalletController.Count-1 Do
    Begin
      NewItem := TMenuItem.Create(Self);
      NewItem.AutoCheck := True;
      NewItem.Caption := PalletController.Pallets[Index].Name;
      NewItem.Checked := (PalletController.Pallets[Index]=PalletController.Active);
      NewItem.GroupIndex := 1;
      NewItem.RadioItem := True;
      NewItem.Tag := Index;
      NewItem.OnClick := @PalletMenuItemClick;
      ParentMenuItem.Add(NewItem);
    End;
End;

Procedure TMainForm.BuildGeneratorMenu(ParentMenuItem: TMenuItem);
Var
  Index: Integer;
  NewItem: TMenuItem;
Begin
  For Index := 0 To GeneratorController.Count-1 Do
    Begin
      NewItem := TMenuItem.Create(Self);
      NewItem.AutoCheck := True;
      NewItem.Caption := GeneratorController.Generators[Index].Name;
      NewItem.Checked := (GeneratorController.Generators[Index]=GeneratorController.Active);
      NewItem.GroupIndex := 2;
      NewItem.RadioItem := True;
      NewItem.Tag := Index;
      NewItem.OnClick := @GeneratorMenuItemClick;
      ParentMenuItem.Add(NewItem);
    End;
End;

End.

