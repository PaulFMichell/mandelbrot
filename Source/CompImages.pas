Unit CompImages;

{ FCL Compact Image Unit (c) 2016, Paul F. Michell. }

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, fpImage;

{$I Defines.inc}

Type
  TCompactColor = Packed Record
    {$IFDEF BGRAIMAGE}
    Blue, Green, Red, Alpha: Byte;
    {$ELSE}
    Red, Green, Blue, Alpha: Byte;
    {$ENDIF}
  End;
  TCompactColorPointer = ^TCompactColor;

  TCompactImage = class(TFPCustomImage)
  Protected
    FData: TCompactColorPointer;
    Function GetInternalColor(x, y: Integer): TFPColor; Override;
    Function GetInternalPixel({%H-}x, {%H-}y: Integer): Integer; Override;
    Procedure SetInternalColor (x, y: Integer; Const Value: TFPColor); Override;
    Procedure SetInternalPixel({%H-}x, {%H-}y: Integer; {%H-}Value: Integer); Override;
  Public
    Destructor Destroy; Override;
    Procedure Clear(Color: TCompactColor);
    Procedure SetSize(AWidth, AHeight: Integer); Override;
    Property Data: TCompactColorPointer Read FData Write FData;
  End;

Implementation

Destructor TCompactImage.Destroy;
Begin
  ReAllocMem(FData, 0);
  Inherited Destroy;
End;

Procedure TCompactImage.SetSize(AWidth, AHeight: Integer);
Begin
  If (AWidth<>Width) Or (AHeight<>Height) Then
    Begin
      ReAllocMem(FData, SizeOf(TCompactColor)*AWidth*AHeight);
      Inherited SetSize(AWidth, AHeight);
    End;
End;

Function TCompactImage.GetInternalColor(x, y: Integer): TFPColor;
Var
  Color: TCompactColor;
Begin
  Color := FData[x+y*Width];
  With Color Do
    Begin
      Result.Red := (Red ShL 8)+Red;
      Result.Green := (Green ShL 8)+Green;
      Result.Blue := (Blue ShL 8)+Blue;
      Result.Alpha := (Alpha ShL 8)+Alpha;
    End;
End;

Function TCompactImage.GetInternalPixel(x, y: Integer): Integer;
Begin
  { Not used by Compact Images. }
  Result := 0;
End;

Procedure TCompactImage.SetInternalColor(x, y: Integer; Const Value: TFPColor);
Var
  Color: TCompactColor;
Begin
  Color.Red := Value.Red ShR 8;
  Color.Green := Value.Green ShR 8;
  Color.Blue := Value.Blue ShR 8;
  Color.Alpha := Value.Alpha ShR 8;
  FData[x+y*Width] := Color;
End;

Procedure TCompactImage.SetInternalPixel(x, y: Integer; Value: Integer);
Begin
  { Not used by Compact Images. }
End;

Procedure TCompactImage.Clear(Color: TCompactColor);
Begin
  FillDWord(FData^, Width*Height, DWord(Color));
End;

End.

