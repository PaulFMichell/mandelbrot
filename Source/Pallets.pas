Unit Pallets;

{$IFDEF FPC}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Graphics, CompImages;

Type
  TPalletController = Class;
  TPallet = Class
  Private
    FController: TPalletController;
    FName: String;
    FPalletSize: Integer;
    Procedure SetPalletSize(Value: Integer);
  Protected
    Property Controller: TPalletController Read FController Write FController;
  Public
    NodeColors: Array Of TColor;
    PalletColors: Array Of LongWord;
    Constructor Create(Name: String);
    Destructor Destroy; Override;
    Procedure Build;
    Property Name: String Read FName;
    Property PalletSize: Integer Read FPalletSize Write SetPalletSize;
  End;
  TPalletController = Class
  Private
    FActive: TPallet;
    FPallets: TFPList;
    Function GetCount: Integer;
    Function GetPallet(Index: Integer): TPallet;
    Procedure SetPallet(Index: Integer; Value: TPallet);
  Public
    Constructor Create;
    Destructor Destroy; Override;
    Procedure Activate(Index: Integer);
    Procedure Add(Name: String; Colors: Array of TColor);
    Property Active: TPallet Read FActive;
    Property Count: Integer Read GetCount;
    Property Pallets[Index: Integer]: TPallet Read GetPallet Write SetPallet;
  End;

Var
  PalletController: TPalletController;

Implementation

Constructor TPallet.Create(Name: String);
Begin
  FName := Name;
  FPalletSize := 256;
End;

Destructor TPallet.Destroy;
Begin
  SetLength(NodeColors, 0);
  SetLength(PalletColors, 0);
  Inherited Destroy;
End;

Procedure TPallet.SetPalletSize(Value: Integer);
Begin
  If FPalletSize<>Value Then
    Begin
      FPalletSize := Value;
      Build;
    End;
End;

Procedure TPallet.Build;
Var
  Index, Node, NextNode, Step: Integer;
  RValue, BValue, GValue: Double;
  NodeCount: Integer;
  StepLimit : Integer;
  Function RGBToPalletColor(Red, Green, Blue: Byte): LongWord;
  Var
    Color: TCompactColor;
  Begin
    Color.Red := Red;
    Color.Green := Green;
    Color.Blue := Blue;
    Color.Alpha := 255;
    Result := LongWord(Color);
  End;
Begin
  NodeCount := Length(NodeColors);
  SetLength(PalletColors, PalletSize);
  Index := 0;
  For Node := 0 To NodeCount-2 Do
    Begin
      NextNode := Node+1;
      StepLimit := Trunc(PalletSize/(NodeCount-1))-1;
      For Step := 0 To StepLimit Do
        Begin
          RValue := ((Step*Red(NodeColors[NextNode]))+((StepLimit-Step)*Red(NodeColors[Node])))/StepLimit;
          GValue := ((Step*Green(NodeColors[NextNode]))+((StepLimit-Step)*Green(NodeColors[Node])))/StepLimit;
          BValue := ((Step*Blue(NodeColors[NextNode]))+((StepLimit-Step)*Blue(NodeColors[Node])))/StepLimit;
          PalletColors[Index] := RGBToPalletColor(Trunc(RValue), Trunc(GValue), Trunc(BValue));
          Inc(Index);
        End;
   End;
End;

Constructor TPalletController.Create;
Begin
  FActive := Nil;
  FPallets := TFPList.Create;
End;

Destructor TPalletController.Destroy;
Var
  Index: Integer;
Begin
  FActive := Nil;
  For Index := FPallets.Count-1 DownTo 0 Do
    TPallet(FPallets.Items[Index]).Free;
  FPallets.Free;
  Inherited Destroy;
End;

Procedure TPalletController.Activate(Index: Integer);
Begin
  FActive := Pallets[Index];
End;

Function TPalletController.GetCount: Integer;
Begin
  Result := FPallets.Count;
End;

Function TPalletController.GetPallet(Index: Integer): TPallet;
Begin
  If (Index>=0) And (Index<FPallets.Count) Then
    Result := TPallet(FPallets.Items[Index])
  Else
    Result := Nil;
End;

Procedure TPalletController.SetPallet(Index: Integer; Value: TPallet);
Begin
  FPallets.Items[Index] := Value;
End;

Procedure TPalletController.Add(Name: String; Colors: Array Of TColor);
Var
  Pallet: TPallet;
  Index: Integer;
Begin
  Pallet := TPallet.Create(Name);
  Pallet.Controller := Self;
  SetLength(Pallet.NodeColors, Length(Colors));
  For Index := 0 To Length(Colors)-1 Do
    Pallet.NodeColors[Index] := Colors[Index];
  Pallet.Build;
  FPallets.Add(Pallet);
End;

Initialization

PalletController := TPalletController.Create;

Finalization

PalletController.Free;

End.

