Program Mandelbrot;

{$IFDEF FPC}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Uses
  {$IFDEF UNIX}
  cmem, cthreads,
  {$ENDIF}
  Interfaces, Forms, lazopenglcontext, Main, SingleGen, BlankGen, DoubleGen,
  SSEGen;

{$R *.res}

Begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
End.

