Unit GLCompImages;

{ FCL Compact Image OpenGL Interface (c) 2016, Paul F. Michell. }

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

{$I Defines.inc}

Uses
  Classes, GL, GLU, GLExt, CompImages;

{$IFDEF BGRAIMAGE}
Const
  GL_BGRA_EXT = $80E1;
{$ENDIF}

Type
  TGLCompactImage = Class
  Private
    FCompactImage: TCompactImage;
  Public
    Constructor Create(CompactImage: TCompactImage);
    Destructor Destroy; Override;
    Procedure DrawToOpenGL(DisplayRect: TRect);
  End;

Implementation

Constructor TGLCompactImage.Create(CompactImage: TCompactImage);
Begin
  FCompactImage := CompactImage;
End;

Destructor TGLCompactImage.Destroy;
Begin
  Inherited Destroy;
End;

Var
  cube_rotationx: GLFloat;
  cube_rotationy: GLFloat;
  cube_rotationz: GLFloat;

Procedure TGLCompactImage.DrawToOpenGL(DisplayRect: TRect);
Begin
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0, FCompactImage.Width, 0, FCompactImage.Height, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glDisable(GL_DEPTH_TEST);
  glViewport(DisplayRect.Left, DisplayRect.Bottom, DisplayRect.Right, DisplayRect.Top);
  {$IFDEF BGRAIMAGE}
  glDrawPixels(FCompactImage.Width, FCompactImage.Height, GL_BGRA_EXT, GL_UNSIGNED_BYTE, FCompactImage.Data);
  {$ELSE}
  glDrawPixels(FCompactImage.Width, FCompactImage.Height, GL_RGBA, GL_UNSIGNED_BYTE, FCompactImage.Data);
  {$ENDIF}
  glRasterPos2i(0, FCompactImage.Height);
  glPixelZoom(1.0, -1.0);
End;

End.

