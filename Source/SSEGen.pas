Unit SSEGen;

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Graphics, GraphType, CompImages, LCLCompImages,
  Generators, Pallets, CPUTime;

Type
  TSSEGenerator = Class(TGenerator)
  Public
    Procedure DoRender(Image: TCompactImage; VR, VI, VS: Double); Override;
  End;

Implementation

Procedure TSSEGenerator.DoRender(Image: TCompactImage; VR, VI, VS: Double);
Var
  M, R, I, I2: Double;
  ZR, ZI: Double;
  ZRL, ZIL: Double;
  K: Integer;
  StartX, MidX, EndX: Integer;
  StartY, MidY, EndY: Integer;
  X, Y: Integer;
  ColorValue: TColor;
  RawImage: TRawImage;
  ScanLinePointer: PByte;
  PixelPointer: PByte;
  BytesPerPixel: Integer;
  BytesPerLine: Integer;
Begin
  RawImage := GeneratorController.ImagePainter.RawImage;
  BytesPerPixel := RawImage.Description.BitsPerPixel Div 8;
  BytesPerLine := RawImage.Description.BytesPerLine;
  ScanLinePointer := RawImage.Data;
  StartX := 0;
  MidX := Image.Width Div 2;
  EndX := Image.Width-1;
  StartY := 0;
  MidY := Image.Height Div 2;
  EndY := Image.Height-1;
  For Y := StartY To EndY Do
    Begin
      PixelPointer := ScanLinePointer;
      I := VI+Double(MidY-Y)*VS;
      I2 := I*I;
      For X := StartX To EndX Do
        Begin
          R := VR+Double(X-MidX)*VS;
          M := R*R+I2;
          If M<4 Then
            Begin
              ColorValue := clBlack;
              ZRL := 0;
              ZIL := 0;
              K := 0;
              Asm
                {$IFDEF CPU64}
                movsd xmm10, ZRL;
                movsd xmm11, ZIL;
                {$ELSE}
                {$ENDIF CPU64}
              End;
              Repeat
                Asm
                  {$IFDEF CPU64}
                  // ZR := ZRL*ZRL-ZIL*ZIL+R;
                  movsd xmm12, xmm10
                  mulsd xmm12, xmm12
                  movsd xmm13, xmm11
                  mulsd xmm13, xmm13
                  subsd xmm12, xmm13
                  addsd xmm12, R
                  movsd ZR, xmm12
                  // ZI := 2*ZRL*ZIL+I;
                  // ZIL := ZI;
                  mulsd xmm11, xmm10
                  addsd xmm11, xmm11  //2*
                  addsd xmm11, I
                  // ZRL := ZR;
                  movsd xmm10, xmm12
                  // M := ZR*ZR+ZI*ZI;
                  mulsd xmm12, xmm12
                  movsd xmm13, xmm11
                  mulsd xmm13, xmm13
                  addsd xmm12, xmm13
                  movsd M, xmm12
                  {$ELSE}
                  {$ENDIF CPU64}
                End;
                If M>=4 Then
                  Begin
                    With PalletController.Active Do
                      ColorValue := PalletColors[Trunc(PalletSize*(K+(4/M))/256)];
                    Break;
                  End;
                Inc(K);
              Until K=256;
            End
          Else
            ColorValue := PalletController.Active.PalletColors[0];
          PInteger(PixelPointer)^ := ColorValue;
          Inc(PixelPointer, BytesPerPixel);
        End;
      Inc(ScanLinePointer, BytesPerLine);
    End;
End;

Var
  Generator: TSSEGenerator;

Initialization

Generator := TSSEGenerator.Create('SSE Assembler');

Finalization

Generator.Free;

End.

