Unit SingleGen;

{$IFDEF FPC}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Graphics, GraphType, CompImages, LCLCompImages,
  Generators, Pallets, CPUTime;

Type
  TSingleGenerator = Class(TGenerator)
  Public
    Procedure DoRender(Image: TCompactImage; VR, VI, VS: Double); Override;
  End;

Implementation

Procedure TSingleGenerator.DoRender(Image: TCompactImage; VR, VI, VS: Double);
Var
  M, R, I, I2: Single;
  ZR, ZI: Single;
  ZRL, ZIL: Single;
  K: Integer;
  StartX, MidX, EndX: Integer;
  StartY, MidY, EndY: Integer;
  X, Y: Integer;
  ColorValue: LongWord;
  RawImage: TRawImage;
  ScanLinePointer: PByte;
  PixelPointer: PByte;
  BytesPerPixel: Integer;
  BytesPerLine: Integer;
Begin
  RawImage := GeneratorController.ImagePainter.RawImage;
  BytesPerPixel := RawImage.Description.BitsPerPixel Div 8;
  BytesPerLine := RawImage.Description.BytesPerLine;
  ScanLinePointer := RawImage.Data;
  StartX := 0;
  MidX := GeneratorController.Image.Width Div 2;
  EndX := GeneratorController.Image.Width-1;
  StartY := 0;
  MidY := GeneratorController.Image.Height Div 2;
  EndY := GeneratorController.Image.Height-1;
  For Y := StartY To EndY Do
    Begin
      PixelPointer := ScanLinePointer;
      I := VI+Single(MidY-Y)*VS;
      I2 := I*I;
      For X := StartX To EndX Do
        Begin
          R := VR+Single(X-MidX)*VS;
          M := R*R+I2;
          If M<4 Then
            Begin
              ColorValue := clBlack;
              ZRL := 0;
              ZIL := 0;
              K := 0;
              Repeat
                ZR := ZRL*ZRL-ZIL*ZIL+R;
                ZI := 2*ZRL*ZIL+I;
                ZRL := ZR;
                ZIL := ZI;
                M := ZR*ZR+ZI*ZI;
                If M>=4 Then
                  Begin
                    With PalletController.Active Do
                      ColorValue := PalletColors[Trunc(PalletSize*(K+(4/M))/256)];
                    Break;
                  End;
                Inc(K);
              Until K=256;
            End
          Else
            ColorValue := PalletController.Active.PalletColors[0];
          PInteger(PixelPointer)^ := ColorValue;
          Inc(PixelPointer, BytesPerPixel);
        End;
      Inc(ScanLinePointer, BytesPerLine);
    End;
End;

Var
  Generator: TSingleGenerator;

Initialization

Generator := TSingleGenerator.Create('Pascal (Single)');

Finalization

Generator.Free;

End.

